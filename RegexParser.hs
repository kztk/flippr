module RegexParser (parseRE, parseClass) where 

import Control.Applicative hiding (many,some)
import Data.Char 

import RegularGrammar hiding (many)
import Text.TDParser 
import Data.CharSet (CharSet)
import qualified Data.CharSet as CS
import Data.Monoid 

import Data.Typeable 

parseRE :: String -> Reg 
parseRE    = either error id . runParser pRE 

parseClass :: String -> CharSet 
parseClass = either error id . runParser pClass

many :: Typeable t => Int -> Parser m Int t -> Parser m Int [t] 
many k p = memoize k $ 
           [] <$ text ""
           <|>
           (:) <$> p <*> many k p 

manyP :: Typeable t => Int -> Parser m Int t -> Parser m Int [t] 
manyP k p = (:) <$> p <*> many k p 

digit :: Parser m Int Int 
digit = foldl (\a b -> a*10 + f b) 0 <$> 
          manyP (-1) (charOf CS.digitChars)
  where
    f b = ord b - ord '0'

hexdigit :: Parser m Int Int           
hexdigit = foldl (\a b -> a*16 + f b) 0 <$>
              manyP (-2) (charOf CS.hexDigitChars)
  where
    f b | '0' <= b && b <= '9' = ord b - ord '0' 
        | 'a' <= b && b <= 'f' = ord b - ord 'a' + 10
        | 'F' <= b && b <= 'F' = ord b - ord 'F' + 10 
        | otherwise            = error "Cannot happen"

sClass :: Parser m Int CharSet                                  
sClass = text "\\" *> f
  where
    f = CS.digitChars <$ text "d"
        <|> (CS.neg CS.digitChars) <$ text "D"
        <|> CS.spaceChars <$ text "s"
        <|> (CS.neg CS.spaceChars) <$ text "s"

pClass :: Parser m Int CharSet   
pClass = foldr (CS.union) (CS.empty) <$> many 0 f
  where 
    f = range <|> sClass <|> singleton 
    range = 
      (\s e -> CS.charSetRange s e) <$> 
       ch <* text "-" <*> ch 
    singleton =
      CS.singleton <$> ch      
    ch = char "[]-\\"

char :: String -> Parser m Int Char          
char esc = 
  (text "\\" *> (escaped <|> number <|> control))
  <|> normal
  where
    escaped = charOf (CS.fromList esc)
    normal  = charOf (CS.neg $ CS.fromList esc)
    number  = chr <$> ((text "0x" *> hexdigit)
                           <|> digit) 
    control = 
          '\n' <$ text "n" 
      <|> '\v' <$ text "v"
      <|> '\t' <$ text "t"
      <|> '\r' <$ text "r"
      <|> '\a' <$ text "a"
      <|> '\b' <$ text "b"
      <|> '\f' <$ text "f"

      
pRE :: Parser m Int Reg       
pRE = memoize 1 f       
  where
    f = ReOr <$> pRE <* text "|" <*> pSeq 
        <|> ReAnd <$> pRE <* text "&" <*> pSeq
        <|> (\x y -> ReAnd x (ReNot y)) <$> pRE <* text "#" <*> pSeq 
        <|> pSeq 

pSeq :: Parser m Int Reg        
pSeq = memoize 2 f         
  where
    f = ReThen <$> pAtom <*> pSeq 
        <|> ReEps <$ text ""
        <|> pAtom 

pAtom :: Parser m Int Reg         
pAtom = memoize 3 f         
  where 
    f = ReCharSet . CS.neg <$ text "[^" <*> pClass <* text "]"
        <|> ReCharSet <$ text "[" <*> pClass <* text "]"
        <|> ReRep <$> pAtom <* text "*"
        <|> (\r -> r <> ReRep r) <$> pAtom <* text "+"
        <|> (\r -> ReOr r ReEps) <$> pAtom <* text "?"
        <|> ReCharSet CS.universal <$ text "." 
        <|> ReCharSet <$> sClass 
        <|> (ReCharSet . CS.singleton) <$>
              char "\\-[]()+?*.|&#"
        <|> text "(" *> pRE <* text ")"
