module Main where 

import Syntax
import Name 

import ParserSyntax 
import Text.WadlerPpr 

import System.Environment 
import System.Exit
import System.IO

import Control.Monad 

import Parser
import qualified CodeGen.Haskell as Hs
import Inverter
import Typing 
import Desugar 

import System.Console.GetOpt 


version :: String 
version = "0.1.2"

predefinedFunctions :: String 
predefinedFunctions = unlines [
  "__line__ = charOf spaceChars <> __linebreak__; ",
  "__linebreak__ = text \"\" <+ __line__; "
  ]

showType :: Show a => (a, [Type], Type) -> String 
showType (n,ts,t) =
    show n ++ " :: " ++ foldr (\x y -> showT x ++ " -> " ++ y) (showT t) ts 
        where
          showT (TyInput)    = "AST"
          showT (TyDocument) = "Doc"
          showT (TyContext)  = "Cond"
          showT (TyCharSet)  = "CharSet" 
          showT (TyReg)      = "StringSet"
          showT TyConstant   = "Static" -- cannot happen 

-- Supported output
data Lang = LangHaskell 

data Options = Options 
  { optVerbose     :: Bool
  , optLanguage    :: Lang 
  , optShowVersion :: Bool
  , optShowHelp    :: Bool 
  , optOutputPpr   :: Maybe FilePath
  , optOutputParse :: Maybe FilePath
  , optImport      :: [String]
  , optInput       :: Maybe FilePath 
  }
  
defaultOptions :: Options 
defaultOptions = Options
  { optVerbose     = False
  , optLanguage    = LangHaskell  
  , optShowVersion = False
  , optShowHelp    = False 
  , optOutputPpr   = Nothing
  , optOutputParse = Nothing 
  , optImport      = []
  , optInput       = Nothing 
  }

options :: [OptDescr (Options -> Options)]
options =
  [ Option ['v']     ["verbose"]
           (NoArg (\ opts -> opts { optVerbose = True }))
           "verbose mode"
  , Option ['V','?'] ["version"]
           (NoArg (\ opts -> opts { optShowVersion = True }))
           "show the version number and quit."
  , Option ['h'] ["help"]
           (NoArg (\ opts -> opts { optShowHelp = True }))
           "show this help message and quit."
  , Option ['i'] ["input"]
           (ReqArg (\ f opts -> opts { optInput = path f }) "FILE")
           "input FILE"
  , Option ['p'] ["pretty"]
           (ReqArg (\f opts -> opts { optOutputPpr = path f} ) "FILE")
           "output pretty-printer FILE"
  , Option ['P'] ["parser"]
           (ReqArg (\f opts -> opts { optOutputParse = path f}) "FILE")
           "output parser FILE"
  , Option ['l'] ["lang"]
           (ReqArg (\_ opts -> opts) "LANG")
           "output LANG. Currently, only \"Haskell\" is supported." 
  , Option ['m'] ["import"]
           (ReqArg (\f opts -> opts { optImport = f : optImport opts }) "Module")
           "importing Module(s) (usually a module defines AST) in parser/pretty-printers."
  ]
  where 
    path "-" = Nothing
    path f   = Just f 
                     
               
usageString :: IO String                
usageString =               
  do progName <- getProgName 
     return $ usageInfo (header progName) options 
   where
     header p = "Usage: " ++ p ++ " [OPTIONS...] FILENAME"

parseArgs :: [String] -> IO Options        
parseArgs args =
  case getOpt Permute options args of
    (o,[], []) -> return $ procOpts o 
    (o,n:_,[]) -> return $ (procOpts o) { optInput = Just n }
    (_,_,errs) -> do { s <- usageString
                     ; ioError $ userError $ concat errs ++ "\n" ++ s }
  where
    procOpts o = foldl (flip ($)) defaultOptions o

-- usage = 
--     do progName <- getProgName 
--        putStrLn "Usage: " 
--        putStrLn $ "    " ++ progName ++ "[OPTIONS] FILENAME" 

readInput :: Maybe FilePath -> IO String
readInput Nothing  = do 
  hPutStrLn stderr "Please specify an input file."
  usageString >>= hPutStrLn stderr
  exitFailure 

  -- getContents
readInput (Just f) = readFile f 

main :: IO () 
main = do { args <- getArgs
          ; opts <- parseArgs args 
          -- show the version number and exit
          ; when (optShowVersion opts) $ 
            do { p <- getProgName 
               ; putStrLn $ p ++ " " ++ "version" 
               ; exitSuccess } 
          -- show the help message and exit
          ; when (optShowHelp opts) $ 
            do { s <- usageString 
               ; putStrLn s 
               ; exitSuccess }
          -- main procedure   
          ; code <- readInput (optInput opts)
          ; let origProgram = parseProg code 
          ; let program     = origProgram ++ parseProg predefinedFunctions 
          ; let tys         = typing program
          ; let (coreProgram, coreTys) = desugar (forgetLayout program, tys)
          ; when (optVerbose opts) $ 
              -- show type information 
              do { let isOriginal (f,_,_) 
                         = f `elem` [ f | Decl _ f _ _ <- origProgram ]
                 ; hPutStr stderr 
                     ((pretty :: Doc -> String) $ 
                        foldDoc (</>) $ map (text . showType) $ 
                          filter isOriginal tys)
                 }
          ; let parseProgram = invert coreProgram coreTys 
          ; case (optLanguage opts) of 
                LangHaskell -> 
                  genHaskell opts origProgram parseProgram }

genHaskell :: Options -> Prog i -> (Name, ParseProg) -> IO () 
genHaskell opts pretty parser =
  case (optOutputPpr opts, optOutputParse opts) of 
    (Just x, Just y) -> 
      do { writeFile x (Hs.codeGenPpr   imps pretty)
         ; writeFile y (Hs.codeGenParse imps parser)}
    (Nothing, Nothing) ->
      putStrLn $ Hs.codeGen imps pretty parser
    (_,_) -> 
      fail "If one of -p or -P is specified, then both of -p and -P must be specified."
  where
    imps = map ("import "++) (optImport opts)
      
  -- do 
  --         ; putStrLn $ "import Examples.Defs\n"
  --                       ++ (codeGen program (invert coreProgram coreTys))
  --         }
