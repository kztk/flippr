-- Similar to comment.txt, but doen in more "smart" way.

pprMain [] = nil; 
pprMain (s : x) = nil <> p s x <> nil;

p s []     = pprInt s;
p s (a:x)  = pprInt s <> sep <> nil <> p a x ; 

-- 
-- text x = text (x @@ anystring)
--
pprInt s = text (bij i2a s @@ re "[0-9]+");

sep = space <+ comments;
comments = nil <+ (nil <> comment <> comments);
space = (text " " <+ charOf (spaceChars `butnot` chars " ")) <> nil;
nil   = text "" <+ space;

-- comment generator 
comment = lineComment <+ blockCommentGen "{-" "-}";
lineComment  = text "--" <> nbstring;
nbstring = text "" <+ charOf (allChars `butnot` char '\n') <> nbstring;

blockCommentGen open close
    = text open <> blockCommentIn open close <> text close;

blockCommentIn open close =
    text ""
    <+ safeChar open close <> blockCommentIn open close
    <+ notStarting open  <> blockCommentIn open close 
    <+ notStarting close <> blockCommentIn open close
    <+ text open <> blockCommentIn open close <> text close <> blockCommentIn open close;
 
safeChar (o:os) (c:cs)
    = charOf (allChars `butnot` (char o `orelse` char c));
notStarting (c:[]) 
    = charOf (allChars `butnot` char c);
notStarting (c:d:ds)
    = charOf (char c) <> notStartingWork d ds; 
notStartingWork c []
    = charOf (allChars `butnot` char c);
notStartingWork c (d:ds)
    = charOf (allChars `butnot` char c) <+ charOf (char c) <> notStartingWork d ds;
