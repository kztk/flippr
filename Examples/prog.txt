ppr x = pprRules x;


pprRules []     = nil;
pprRules (r:rs) = nil <> pRules r rs <> nil; 

pRules r' []     = pprRule r' <> (nil <> text ";" <+ nil);
pRules r' (r:rs) = pprRule r' <> nil <> text ";" <> line' <> pRules r rs;


pprRule (Rule f ps e) =
  group (var f <> space <> pprPats ps <> space' <> text "=" 
         <> nest 4 (line' <> pprExp e));
  

pprPats [] = text "";
pprPats (p:ps) = pPats p ps ;

pPats p' []     = pprPat p';
pPats p' (p:ps) = pprPat p' <> space <> pPats p ps;


pprPat p = pprPatPrec 9 p;

pprPatPrec i p = manyParens (pprPat_ i p);

pprPat_ i (PVar x)    = var x;
pprPat_ i (PCon c ps) = patCon i (bij constr c) ps;

patCon i ConCons (p1 : p2 : []) =
  parensIf (i >= 5)
    (pprPatPrec 5 p1 <> space' <> text ":" <> space' <> pprPatPrec 4 p2);
patCon i c [] = con c;
patCon i c (p : ps) = parensIf (i >= 9) (con c <> space <> pPats p ps);

pprExp e = go 0 e;

go i e = manyParens (go_ i e);
go_ i (ECon c es) = goCon i (bij constr c) es;
go_ i (EOp OAlt e1 e2) = parensIf (i >= 5) $ group $ 
    go 5 e1 <> nest 2 (line' <> text "<+" <> space' <> go 4 e2);
go_ i (EOp OCat e1 e2) = parensIf (i >= 6) $ group $ 
    go 6 e1 <> nest 2 (line' <> text "<>" <> space' <> go 5 e2);
go_ i (EOp OLeq e1 e2) = parensIf (i >= 4) $ group $
    go 4 e1 <> nest 2 (line' <> text "<=" <> space' <> go 3 e2);
go_ i (EOp OGeq e1 e2) = parensIf (i >= 4) $ group $
    go 4 e1 <> nest 2 (line' <> text ">=" <> space' <> go 3 e2);
go_ i (EVar f []) = var f;  
go_ i (EVar f (e:es)) = parensIf (i >= 9) (var f <> space <> pExps e es);
-- Redundant pattern in the printing direction, but used for parsing
go_ i (EVar f (e1 : e2 : [])) =
  parensIf (i >= 8)
    (go 8 e1 <> space <> text "`" <> var f <> text "`" <> space <> go 7 e2);
go_ i (EString s) = str s;
go_ i (EChar c) = ch c;
go_ i (EInt n) = int n;
go_ i (EReify e1 e2) =
  text "text" <> nil <> parens (go_ 0 e1 <>
    nest 2 (line' <> text "@@" <> space' <> go_ 0 e2));

pExps e' []     = go 9 e' <+ nil <> text "$" <> nil <> go 0 e';
pExps e' (e:es) = go 9 e' <> space <> pExps e es;
  
goCon i ConCons (e1 : e2 : []) = parensIf (i >= 4)
  (go 4 e1 <> space' <> text ":" <> space' <> go 3 e2);
goCon i c [] = con c;
goCon i c (e : es) = parensIf (i >= 9) (con c <> space <> pExps e es);

var f = text (f @@ re "[a-z_][_a-zA-Z0-9]*'*");
con (ConString c) = text (c @@ re "[A-Z][_a-zA-Z0-9]*'*");
con ConNil = text "[]";
str s = text (bij showRead s @@ re "\"([ !#-\\[\\]-~]|\\\\([abfnrtv\\\\\"'&]|[0-9]+|o[0-7]+|h[0-9A-Fa-f]+))*\"");
int n = text (bij showRead n @@ re "\\d+");
ch c = text (bij showRead c @@ re "'([ -&(-\\[\\]-~]|\\\\([abfnrtv\\\\\"']|[0-9]+|o[0-7]+|h[0-9A-Fa-f]+))'");

parensIf b d = if b then parens d else d ;
manyParens d = d <+ parens (manyParens d);

parens d = text "(" <> nest 1 (nil <> d <> nil) <> text ")";
space = (text " " <+ charOf (spaceChars `butnot` char ' ')) <> nil;
nil = text "" <+ (charOf spaceChars <> nil);

line' = line <+ text "" <+ text "--" <> eol;
space' = space <+ text "";

eol = text "\n" <+ charOf (printChars `butnot` char '\n') <> eol
