{-# OPTIONS_GHC -XDeriveDataTypeable -XRank2Types -XGADTs #-}
module Examples.Defs where 

import Data.Typeable 
import qualified Data.Set as S

import Util

data Bin = Lf | Nd Bin Bin deriving (Show, Typeable, Eq)


data Dir = L | R deriving Eq 
data E = Int Int
       | Var String
       | Minus E E
       | Div E E
       | Add E E
       | IfZ E E E
       | One
       | Sub E E 
       | Zero deriving (Show, Typeable, Eq) 

showRead :: (Read a, Show a) => Bij a String
showRead = Bij show read

constr :: Bij String Con
constr = Bij strToCon conToStr
  where
    strToCon "(:)" = ConCons
    strToCon "[]" = ConNil
    strToCon c = ConString c
    conToStr ConCons = "(:)"
    conToStr ConNil = "[]"
    conToStr (ConString c) = c

data Ex a = ExVar a String 
          | ExInt a Int 
          | ExLet a String (Ex a) (Ex a) 
          | ExSub a (Ex a) (Ex a)
            deriving (Show, Typeable, Eq)
                     
                     
type Prog = [Rule]                     
data Rule = Rule String [Pat] Exp deriving (Show,Typeable,Eq)
data Exp
  = EChar Char
  | ECon String [Exp]
  | EInt Int
  | EOp Op Exp Exp
  | EReify Exp Exp
  | EString String
  | EVar String [Exp]
  deriving (Show, Typeable, Eq)

data Con
  = ConString String
  | ConNil
  | ConCons
  deriving (Show, Typeable, Eq)

data Pat
  = PVar String
  | PCon String [Pat]
  deriving (Show, Typeable, Eq)

data Op   = OCat | OAlt | OLeq | OGeq  deriving (Show,Typeable,Eq)

prog = [ Rule "snoc" [PCon "[]" [], PVar "b"]
               (ECon "(:)" [EVar "b" [], ECon "[]" []]),
         Rule "snoc" [PCon "(:)" [PVar "a", PVar "x"], PVar "b"]
               (ECon "(:)" [EVar "b" [], EVar "snoc" [EVar "x" [], EVar "a" []]])]
