{-# LANGUAGE DeriveDataTypeable #-}
module Examples.LambdaDefs where

import Data.Typeable

type Var = String
type Con = String

data Exp
  = ELet Var Exp Exp
  | EApp Exp Exp
  | EFun Var Exp
  | ECon Con [Exp]
  | EMatch Exp [Alt]
  | EVar Var
  deriving (Eq, Ord, Show, Typeable)

data Pat = Pat Con [Var]
  deriving (Eq, Ord, Show, Typeable)

data Alt = Alt Pat Exp
  deriving (Eq, Ord, Show, Typeable)

data Decl = Decl Var Exp
  deriving (Eq, Ord, Show, Typeable)

type Decls = [Decl]
