%Memo 

You can make an HTML-version of the memo by using the following command.

    make Memo.html


TODO lists
----------

  * Structuralize the modules. Flat modules would not be suitable when we distribute it.  


  * Investigate whether current type system is OK or not.
    Currently, we have the following four types:

      * Input represents an input of pretty-printer. 
        They can be pattern matched, but not
        any argument of this type of a function must be variable.

      * Doc represents the "smart datatype" used in Wadler's ppr library.
        They cannot be pattern matched. 

      * Context represents the contextual information.
        They can be pattern matched, and more over
        any argument of this type of a function must be variable or 
        a constant. 

      * Constant represents a syntactic constant in expressions. 
        like "string", 123, L, [1].
    
    Is above enough? For example, currently we prohibits pattern matching for 
    contextual information, because it requires subtyping. Is it really OK?


In Future Release?
------------------

  * How about introducing pattern-guarads? This enables us to investigate 
    a subtree before traversing it. This can be eliminated by specialization 
    technique.

  * Allow parsers to have enumerators in more compact form. 
    Or, we would like to freeze the computation like:

         f <$> x1 <*> ... <*> xn 

    because such data can be shared an memoized. 

    Any good datatype definitions?


  * Allow higher-order functions, assuming that we rarely use deterministic 
    function values, is it possible to include higher-order functions in the 
    language, with some appropriate type system? 
      
    For example, prohibiting partial-applications and lambdas is enough?



Done
----

  * Chaneg the syntax:
   

         x @@ stringset -> text (x @@ stringset)


    Now, `text x` is a syntax sugar for text `(x @@ anyString)`.

    But, how can we specify stringSet?


         -- syntax for stringSet 
 
         data Reg = ReChar Char
                  | ReThen Reg Reg          -- "followed by" Relation
                  | ReOr   Reg Reg          -- Choice
                  | ReRep  Reg              -- Kleene Star
                  | ReEps                   -- Empty Sequence (unit of ReThen)
                  | ReNull                  -- Empty Set (unit of ReOr)
                  | ReAnd  Reg Reg 
                  | ReNot  Reg              -- Complimentation

         -- Tree Automatom 
         ...

         compile :: Reg -> Automaton 
         ...
         -- Stateful to avoid capturing 

         toCFG :: Automaton -> State Int (Name, ParseProg)
         ...


    Fancy syntax?
    
         (re "[A-Z]*[a-z]" \\ re "A|B|D") `union` re "ABFE"
  * Is it possible to define a function like:
 
        blockCommentGen open close = text open <> blockCommentIn open close <> text clos e;
        blockCommentIn open close =
                 text "";
                 <+ safeChar open close <> blockCommentIn open close
                 <+ notStarting open  <> blockCommentIn open close
                 <+ notStarting close <> blockCommentIn open close
                 <+ text open <> blockCommentIn open close <> text close
                          <> blockCommentIn open close;
        safeChar (o:os) (c:cs) = charOf (anyChar `butnot` (is o `orelse` is c)) ;
        notStarting (c:[]) 
            = charOf (anyChar `butnot` is c);
        notStarting (c:d:ds)
            = charOf (is c) <> notStartingWork d ds; 
        notStartingWork c []
            = charOf (anyChar `butnot` is c);
        notStartingWork c (d:ds)
            = charOf (anyChar `butnot` is c) <+ charOf (is c) <> notStartingWork d ds;

    Note that `blockCommentIn` just prints "" and `blockCommentGen` prints
    `open ++ close` in pretty printing.

    Currently, this kind of program is not allowed because of the two reasons.

      * The current system prohibits pattern matching on "contextual information" parts.
        But, essetially we can allow this because it does not violate the finiteness   
        of the "contextual information"s.

      * The current system restricts some operators must take an constant.
        But, this restriction is also not essential; contextual information should 
        be allowed. Recall that they are eliminted to constant or something after
        desugaring. 

    Can we change the current type inference system as follows?

      * Allow pattern-matching contextual-information. Thus, matching only ensures
        that its introducing variable has the same sort as the pattern.
        Then, we ensure that Doc is not pattern matched by post-process. 


  * Allow _ in patten 
  
       f (A _ x) = exp 
     
    so that source-code location information is inserted to _ position.
    
    A position can be represented by two triples.
       starting position, starting line number, starting column number, 
       ending   position, ending   line number, ending   column number.

    It is not difficult to extract *current* position, line number and 
    column number, a possible solution would be writing 

         (\(p1,e,p2) ->    ) <$>
             extractPos <*> someCode <*> extractPos 

    I am not sure whether this encoding of the information make it difficult 
    to port the system to other parsers. 


  * Allow invertible functions in the program.
    For example, if we write bij a2i, it uses bij a2i in ppr 
    and bij_i a2i in parsing; where users are responsible to the definition of 
    both direction. An intended use is 

         ppr (Int i) = reify numbers (bij a2i i)

    where we assume that bij and bij_i are field names for the following datatype.

         data Bij a b = Bij {bij :: a -> b, bij_i :: b -> a }
    
    Any other good notations?    

  * Allow parses to return a parsed string in addition to usual parsing 
    results. Note that this parsed string is less likely required compared 
    with parsing results, which is used to implement 
    
             d = ...
             ppr(Var x) = reify d x
                                
    Thus, we have to use lazy evaluation for the purpose.                                       


    In addition, it would be better to provide a special notations like
    
         charOf cond

    that works as 

         text $ (:[]) $ head $ filter cond  [minBound :: Char .. maxBound :: Char ]

    in pretty-printing and works as 

         TDParser.charOf cond 

    that accepts any character satisfying the condition.


    
    Then, we can define like 
      
         alphaNums = text "" <+ charOf isAlphaNum <> alphaNums 
         d = charOf isAlpha <> alphaNums 
         ppr(Var x) = reify d x

    One might think that alphaNums should be written in the form of
  
         alphaNums = many (charOf isAlphaNum) 

    However, the function `many`, which can be defined as 

         many d = text "" <+ d <> many d 

    does not satisfy the linearity restriction. We are concerning that introducing 
    operators like `many` would confuse users.

  * Allow parsers to report more detailed parse errors.
    For example, we would a paper to make an error message like

       Parse Error at Line .. Column .. near ...
