{-# LANGUAGE DeriveDataTypeable #-}
{-|
This module provides the datatype for identifiers.
-}

module Name where 

import Data.Typeable 
import Text.WadlerPpr

-- | The |Name| datatype represents identifiers
newtype Name = Name String deriving (Eq,Ord,Typeable)
          
instance Show Name where 
    show (Name s) = s 


instance Pretty Name where
  ppr (Name s) = text s 
