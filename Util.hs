module Util where 

import Data.Typeable 
import Data.Dynamic 

import Data.Maybe 

import Text.WadlerPpr 

type CodeSpan = ( (Int,Int,Int), -- Starting position, line number, and column number
                  (Int,Int,Int)  -- Ending   position, line number, and column number 
                ) 

ignoreU :: a -> () 
ignoreU _ = () 
           
charOf :: (Char -> Bool) -> Doc
charOf f = text $ (:[]) $ head $ filter f [ minBound :: Char .. maxBound :: Char ]

andalso :: (a -> Bool) -> (a -> Bool) -> a -> Bool 
andalso f g x = f x && g x

orelse :: (a -> Bool) -> (a -> Bool) -> a -> Bool 
orelse  f g x = f x || g x

butnot :: (a -> Bool) -> (a -> Bool) -> a -> Bool
butnot  f g x = f x && not (g x)

anyChar :: a -> Bool 
anyChar _ = True

none :: a -> Bool 
none    _ = False

is :: Char -> (Char -> Bool) 
is c = (== c) 
           
infixr 5 <+

(<+) :: a -> b -> a   
x <+ _ = x
infixr 8 @@

(@@) :: String -> a -> Doc   
x @@ _ = text x

data Bij a b = Bij { bij :: a -> b, inv :: b -> a }

invMap ::  (Typeable b, Typeable a) =>
           Bij a b -> [(t, Dynamic)] -> [(t, Dynamic)]
invMap n l = [ (x, (toDyn . inv n . fromJust . fromDynamic) y ) | (x,y) <- l ]
